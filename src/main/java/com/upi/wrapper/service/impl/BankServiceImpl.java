package com.upi.wrapper.service.impl;

import com.m2p.root.exception.ApiException;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.profile.ListKeysDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.bank.AccPvdRespDto;
import com.upi.common.dto.response.bank.BankDto;
import com.upi.common.dto.response.util.CheckSimBindResponseDto;
import com.upi.common.dto.response.util.SimBindReqResponseDto;
import com.upi.common.enums.BooleanType;
import com.upi.common.schema.RespListKeys;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.integrator.impl.ProfileServiceIntegratorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankServiceImpl {

    @Autowired
    ProfileServiceIntegratorImpl profileServiceIntegrator;

    public UpiResponse<List<BankDto>> listBanks(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.listBanks(req);
    }

    public UpiResponse<List<AccPvdRespDto>> listAccountProviders(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.listAccountProviders(req);
    }

    public UpiResponse<BooleanType> checkDevice(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.checkDevice(req);
    }

    public UpiResponse<AbstractResponse> listKeysReq(ListKeysDto req) throws ApiException {
        return profileServiceIntegrator.listKeysReq(req);
    }

    public UpiResponse<RespListKeys.KeyList> listKeysRes(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.listKeysRes(req);
    }

    public UpiResponse<CheckSimBindResponseDto> checkSimBindStatus(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.checkSimBindStatus(req);
    }

    public UpiResponse<SimBindReqResponseDto> simBindReq(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.simBindReq(req);
    }

}
