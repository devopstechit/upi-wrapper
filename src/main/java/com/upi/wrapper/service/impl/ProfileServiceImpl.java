package com.upi.wrapper.service.impl;

import com.m2p.root.exception.ApiException;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.account.AccountFetchReqDto;
import com.upi.common.dto.request.npci.AccountDto;
import com.upi.common.dto.request.npci.AccountReqDto;
import com.upi.common.dto.request.npci.AddressDto;
import com.upi.common.dto.request.profile.BindSimDto;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.vpa.VpaReqDto;
import com.upi.common.dto.response.npci.AccountFetchResDto;
import com.upi.common.enums.BooleanType;
import com.upi.common.schema.RespTypeValAddr;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.integrator.impl.ProfileServiceIntegratorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl {
    @Autowired
    ProfileServiceIntegratorImpl profileServiceIntegrator;

    public UpiResponse<String> validateAddReq(AddressDto req) throws ApiException {
        return profileServiceIntegrator.validateAddReq(req);
    }

    public UpiResponse<RespTypeValAddr> validateAddRes(ProfileDto req) throws ApiException {
        return profileServiceIntegrator.validateAddRes(req);
    }

    public UpiResponse<List<AccountDto>> listAccounts(ProfileDto req) throws ApiException {
        return profileServiceIntegrator.listAccounts(req);
    }

    public UpiResponse<String> addAccount(AccountReqDto req) throws ApiException {
        return profileServiceIntegrator.addAccount(req);
    }

    public UpiResponse<String> setPrimaryAccount(AccountReqDto req) throws ApiException {
        return profileServiceIntegrator.setPrimaryAccount(req);
    }

    public UpiResponse<String> getAccountDetails(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.getAccountDetails(req);
    }

    public UpiResponse<String> accountFetchReq(AccountFetchReqDto req) throws ApiException {
        return profileServiceIntegrator.accountFetchReq(req);
    }

    public UpiResponse<AccountFetchResDto> accountFetchRes(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.accountFetchRes(req);
    }

    public UpiResponse<String> deleteAccount(AccountReqDto req) throws ApiException {
        return profileServiceIntegrator.deleteAccount(req);
    }

    public UpiResponse<String> setPrimaryUpiId(VpaReqDto req) throws ApiException {
        return profileServiceIntegrator.setPrimaryUpiId(req);
    }

    public UpiResponse<String> addNewVpa(VpaReqDto req) throws ApiException {
        return profileServiceIntegrator.addNewVpa(req);
    }

    public UpiResponse<String> vpaAction(VpaReqDto req, String action) throws ApiException {
        return profileServiceIntegrator.vpaAction(req, action);
    }

    public UpiResponse<String> accountAction(AccountReqDto req, String action) throws ApiException {
        return profileServiceIntegrator.accountAction(req, action);
    }

    public UpiResponse<String> checkUpiPort(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.checkUpiPort(req);
    }

    public UpiResponse<String> createUpiPort(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.createUpiPort(req);
    }

    public UpiResponse<String> linkAccount(AbstractRequestDto req) throws ApiException {
        return profileServiceIntegrator.linkAccount(req);
    }

    public UpiResponse<String> checkIfVpaAvailable(VpaReqDto req) throws ApiException {
        return profileServiceIntegrator.checkIfVpaAvailable(req);
    }

    public UpiResponse<BooleanType> bindSim(BindSimDto req) throws ApiException {
        return profileServiceIntegrator.bindSim(req);
    }


}
