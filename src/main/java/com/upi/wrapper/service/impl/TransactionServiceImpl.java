package com.upi.wrapper.service.impl;

import com.m2p.root.exception.ApiException;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.transaction.CheckBalanceReqDto;
import com.upi.common.dto.request.transaction.CheckTransactionReqDto;
import com.upi.common.dto.request.transaction.ReqPayDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.transaction.CheckBalanceRespDto;
import com.upi.common.dto.response.transaction.CheckTxnStatusRespDto;
import com.upi.common.dto.response.transaction.ReqPayRespDto;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.integrator.impl.TransactionServiceIntegratorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl {
    @Autowired
    TransactionServiceIntegratorImpl transactionServiceIntegrator;

    public UpiResponse<AbstractResponse> checkTxnReq(CheckTransactionReqDto req) throws ApiException {
        return transactionServiceIntegrator.checkTxnReq(req);
    }

    public UpiResponse<CheckTxnStatusRespDto> checkTxnRes(ProfileDto req) throws ApiException {
        return transactionServiceIntegrator.checkTxnRes(req);
    }

    public UpiResponse<AbstractResponse> reqPayReq(ReqPayDto req) throws ApiException {
        return transactionServiceIntegrator.reqPayReq(req);
    }

    public UpiResponse<ReqPayRespDto> reqPayRes(ProfileDto req) throws ApiException {
        return transactionServiceIntegrator.reqPayRes(req);
    }


    public UpiResponse<AbstractResponse> checkBalanceReq(CheckBalanceReqDto req) throws ApiException {
        return transactionServiceIntegrator.checkBalanceReq(req);
    }

    public UpiResponse<CheckBalanceRespDto> checkBalanceRes(ProfileDto req) throws ApiException {
        return transactionServiceIntegrator.checkBalanceRes(req);
    }
}
