package com.upi.wrapper.service.impl;

import com.m2p.root.exception.ApiException;
import com.upi.common.dto.request.profile.RegisterPPIProfileDto;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.integrator.impl.ProfileServiceIntegratorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PPIServiceImpl {
    @Autowired
    ProfileServiceIntegratorImpl profileServiceIntegrator;

    public UpiResponse<String> registerPPIProfile(RegisterPPIProfileDto req) throws ApiException {
        return profileServiceIntegrator.registerPPIProfile(req);
    }
}
