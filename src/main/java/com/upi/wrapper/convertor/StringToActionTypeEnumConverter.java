package com.upi.wrapper.convertor;

import com.upi.common.enums.ActionType;
import org.springframework.core.convert.converter.Converter;

public class StringToActionTypeEnumConverter implements Converter<String, ActionType> {
    @Override
    public ActionType convert(String source) {
        return ActionType.valueOf(source.toUpperCase());
    }
}
