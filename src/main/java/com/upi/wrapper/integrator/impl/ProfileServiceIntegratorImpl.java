package com.upi.wrapper.integrator.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.m2p.root.constant.common.M2PRootExceptionCodeConstants;
import com.m2p.root.constant.common.M2PRootExceptionMessageConstants;
import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.m2p.root.integrator.remote.service.RemoteService;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.account.AccountFetchReqDto;
import com.upi.common.dto.request.npci.AccountDto;
import com.upi.common.dto.request.npci.AccountReqDto;
import com.upi.common.dto.request.npci.AddressDto;
import com.upi.common.dto.request.profile.BindSimDto;
import com.upi.common.dto.request.profile.ListKeysDto;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.profile.RegisterPPIProfileDto;
import com.upi.common.dto.request.vpa.VpaReqDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.bank.AccPvdRespDto;
import com.upi.common.dto.response.bank.BankDto;
import com.upi.common.dto.response.npci.AccountFetchResDto;
import com.upi.common.dto.response.util.CheckSimBindResponseDto;
import com.upi.common.dto.response.util.SimBindReqResponseDto;
import com.upi.common.enums.BooleanType;
import com.upi.common.schema.RespListKeys;
import com.upi.common.schema.RespTypeValAddr;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.contants.common.ExceptionCodeConstants;
import com.upi.wrapper.contants.common.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class ProfileServiceIntegratorImpl {

    @Autowired
    ObjectMapper objectMapper;
    @Value("${upi.profile.url.base}")
    private String baseUrl;
    @Value("${upi.profile.url.list_banks}")
    private String listBanksPath;
    @Value("${upi.profile.url.list_accounts}")
    private String listAccountsPath;
    @Value("${upi.profile.url.set_primary_account}")
    private String setPrimaryAccountPath;
    @Value("${upi.profile.url.get_account_details}")
    private String getAccountDetailsPath;
    @Value("${upi.profile.url.account_fetch_req}")
    private String accountFetchReqPath;
    @Value("${upi.profile.url.account_fetch_res}")
    private String accountFetchResPath;
    @Value("${upi.profile.url.delete_account}")
    private String deleteAccountPath;
    @Value("${upi.profile.url.set_primary_upi_id}")
    private String setPrimaryUpiIdPath;

    @Value("${upi.profile.url.add_new_vpa}")
    private String addNewVpaPath;

    @Value("${upi.profile.url.check_upi_port}")
    private String checkUpiPortPath;
    @Value("${upi.profile.url.create_upi_port}")
    private String createUpiPortPath;
    @Value("${upi.profile.url.link_account}")
    private String linkAccountPath;
    @Value("${upi.profile.url.check_if_vpa_available}")
    private String checkIfVpaAvailablePath;
    @Value("${upi.profile.url.list_account_providers}")
    private String listAccountProvidersPath;
    @Value("${upi.profile.url.check_device}")
    private String checkDevicePath;

    @Value("${upi.profile.url.validate_add_req}")
    private String validateAddReqPath;
    @Value("${upi.profile.url.validate_add_res}")
    private String validateAddResPath;
    @Value("${upi.profile.url.vpa_action}")
    private String vpaActionPath;

    @Value("${upi.profile.url.account_action}")
    private String accountActionPath;

    @Value("${upi.profile.url.register_ppi_profile}")
    private String registerPPIProfilePath;

    @Value("${upi.profile.url.add_account}")
    private String addAccountPath;

    @Value("${upi.profile.url.list_keys_req}")
    private String listKeysReqPath;

    @Value("${upi.profile.url.list_keys_res}")
    private String listKeysResPath;

    @Value("${upi.profile.url.check_sim_bind_status}")
    private String checkSimBindStatusPath;

    @Value("${upi.profile.url.sim_bind_req}")
    private String simBindReqPath;

    @Value("${upi.profile.url.sim_bind}")
    private String simBindPath;

    @Autowired
    private ApiExceptionBuilder exceptionBuilder;
    @Autowired
    private RemoteService service;

    @Autowired
    private RestTemplate restTemplate;

/*    private <T> T postData(AbstractRequestDto req, String path) throws ApiException {
        String url = baseUrl + path;
        try {
            return fetchData(url, objectMapper.writeValueAsString(req),  new ParameterizedTypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.JSON_PROCESSING_EXCEPTION, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
        }
    }*/

//    private <T> T postData(AbstractRequestDto req, String path, ParameterizedTypeReference<T> param) throws ApiException {
//        String url = baseUrl + path;
//        try {
//            return fetchData(url, objectMapper.writeValueAsString(req), param);
//        } catch (JsonProcessingException e) {
//            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.JSON_PROCESSING_EXCEPTION, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
//        }
//    }

    public UpiResponse<String> validateAddReq(AddressDto req) throws ApiException {
        return postData(req, validateAddReqPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<RespTypeValAddr> validateAddRes(ProfileDto req) throws ApiException {
        return postData(req, validateAddResPath, new ParameterizedTypeReference<UpiResponse<RespTypeValAddr>>() {
        });
    }

    public UpiResponse<List<BankDto>> listBanks(AbstractRequestDto req) throws ApiException {
        return postData(req, listBanksPath, new ParameterizedTypeReference<UpiResponse<List<BankDto>>>() {
        });
    }

    public UpiResponse<List<AccPvdRespDto>> listAccountProviders(AbstractRequestDto req) throws ApiException {
        return postData(req, listAccountProvidersPath, new ParameterizedTypeReference<UpiResponse<List<AccPvdRespDto>>>() {
        });
    }

    public UpiResponse<List<AccountDto>> listAccounts(ProfileDto req) throws ApiException {
        return postData(req, listAccountsPath, new ParameterizedTypeReference<UpiResponse<List<AccountDto>>>() {
        });
    }

    public UpiResponse<String> setPrimaryAccount(AccountReqDto req) throws ApiException {
        return postData(req, setPrimaryAccountPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> addAccount(AccountReqDto req) throws ApiException {
        return postData(req, addAccountPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> getAccountDetails(AbstractRequestDto req) throws ApiException {
        return postData(req, getAccountDetailsPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> accountFetchReq(AccountFetchReqDto req) throws ApiException {
        return postData(req, accountFetchReqPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<AccountFetchResDto> accountFetchRes(AbstractRequestDto req) throws ApiException {
        return postData(req, accountFetchResPath, new ParameterizedTypeReference<UpiResponse<AccountFetchResDto>>() {
        });
    }

    public UpiResponse<String> deleteAccount(AccountReqDto req) throws ApiException {
        return postData(req, deleteAccountPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> setPrimaryUpiId(VpaReqDto req) throws ApiException {
        return postData(req, setPrimaryUpiIdPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> addNewVpa(VpaReqDto req) throws ApiException {
        return postData(req, addNewVpaPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> vpaAction(VpaReqDto req, String action) throws ApiException {
        return postData(req, vpaActionPath + '/' + action, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> accountAction(AccountReqDto req, String action) throws ApiException {
        return postData(req, accountActionPath + '/' + action, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<BooleanType> checkDevice(AbstractRequestDto req) throws ApiException {
        return postData(req, checkDevicePath, new ParameterizedTypeReference<UpiResponse<BooleanType>>() {
        });
    }

    public UpiResponse<String> checkUpiPort(AbstractRequestDto req) throws ApiException {
        return postData(req, checkUpiPortPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> createUpiPort(AbstractRequestDto req) throws ApiException {
        return postData(req, createUpiPortPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> linkAccount(AbstractRequestDto req) throws ApiException {
        return postData(req, linkAccountPath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> checkIfVpaAvailable(VpaReqDto req) throws ApiException {
        return postData(req, checkIfVpaAvailablePath, new ParameterizedTypeReference<UpiResponse<String>>() {
        });
    }

    public UpiResponse<String> registerPPIProfile(RegisterPPIProfileDto req) throws ApiException {
        return postData(req, registerPPIProfilePath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<AbstractResponse> listKeysReq(ListKeysDto req) throws ApiException {
        return postData(req, listKeysReqPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<RespListKeys.KeyList> listKeysRes(AbstractRequestDto req) throws ApiException {
        return postData(req, listKeysResPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<CheckSimBindResponseDto> checkSimBindStatus(AbstractRequestDto req) throws ApiException {
        return postData(req, checkSimBindStatusPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<SimBindReqResponseDto> simBindReq(AbstractRequestDto req) throws ApiException {
        return postData(req, simBindReqPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<BooleanType> bindSim(BindSimDto req) throws ApiException {
        return postData(req, simBindPath, new ParameterizedTypeReference<>() {
        });
    }

    private <T, U> T postData(U req, String path, ParameterizedTypeReference<T> param) throws ApiException {
        String url = baseUrl + path;
        try {
            return fetchData(url, objectMapper.writeValueAsString(req), param);
        } catch (JsonProcessingException e) {
            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.JSON_PROCESSING_EXCEPTION, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
        }
    }

    private <T> T fetchData(String path, String value, ParameterizedTypeReference<T> param) throws ApiException {
        T response = service.postData(value, path, param, setHeaders(), restTemplate, exceptionBuilder);
        log.info(String.format(MessageConstants.RESPONSE_FROM_UPI_PROFILE, response));
        if (response == null) {
            log.error(ExceptionCodeConstants.EMPTY_RESPONSE_FROM_UPI_PROFILE);
            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.REMOTE_CALL_EMPTY_RESPONSE, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
        }
        return response;
    }

    private HttpHeaders setHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
