package com.upi.wrapper.integrator.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.m2p.root.constant.common.M2PRootExceptionCodeConstants;
import com.m2p.root.constant.common.M2PRootExceptionMessageConstants;
import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.m2p.root.integrator.remote.service.RemoteService;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.transaction.CheckBalanceReqDto;
import com.upi.common.dto.request.transaction.CheckTransactionReqDto;
import com.upi.common.dto.request.transaction.ReqPayDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.transaction.CheckBalanceRespDto;
import com.upi.common.dto.response.transaction.CheckTxnStatusRespDto;
import com.upi.common.dto.response.transaction.ReqPayRespDto;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.contants.common.ExceptionCodeConstants;
import com.upi.wrapper.contants.common.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;


@Slf4j
@Service
public class TransactionServiceIntegratorImpl {
    @Autowired
    ObjectMapper objectMapper;

    @Value("${upi.transaction.url.base}")
    private String baseUrl;

    @Value("${upi.transaction.url.check_txn_req}")
    private String checkTxnReqPath;

    @Value("${upi.transaction.url.check_txn_res}")
    private String checkTxnResPath;

    @Value("${upi.transaction.url.req_pay}")
    private String reqPayPath;

    @Value("${upi.transaction.url.req_pay_res}")
    private String reqPayResPath;

    @Value("${upi.transaction.url.check_bal_req}")
    private String checkBalReqPath;

    @Value("${upi.transaction.url.check_bal_res}")
    private String checkBalResPath;

    @Autowired
    private ApiExceptionBuilder exceptionBuilder;

    @Autowired
    private RemoteService service;

    @Autowired
    private RestTemplate restTemplate;

    public UpiResponse<AbstractResponse> checkTxnReq(CheckTransactionReqDto req) throws ApiException {
        return postData(req, checkTxnReqPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<CheckTxnStatusRespDto> checkTxnRes(ProfileDto req) throws ApiException {
        return postData(req, checkTxnResPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<AbstractResponse> reqPayReq(ReqPayDto req) throws ApiException {
        return postData(req, reqPayPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<ReqPayRespDto> reqPayRes(ProfileDto req) throws ApiException {
        return postData(req, reqPayResPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<AbstractResponse> checkBalanceReq(CheckBalanceReqDto req) throws ApiException {
        return postData(req, checkBalReqPath, new ParameterizedTypeReference<>() {
        });
    }

    public UpiResponse<CheckBalanceRespDto> checkBalanceRes(ProfileDto req) throws ApiException {
        return postData(req, checkBalResPath, new ParameterizedTypeReference<>() {
        });
    }
    private <T> T postData(AbstractRequestDto req, String path, ParameterizedTypeReference<T> param) throws ApiException {
        String url = baseUrl + path;
        try {
            return fetchData(url, objectMapper.writeValueAsString(req), param);
        } catch (JsonProcessingException e) {
            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.JSON_PROCESSING_EXCEPTION, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
        }
    }

    private <T> T fetchData(String path, String value, ParameterizedTypeReference<T> param) throws ApiException {
        T response = service.postData(value, path, param, setHeaders(), restTemplate, exceptionBuilder);
        log.info(String.format(MessageConstants.RESPONSE_FROM_UPI_PROFILE, response));
        if (response == null) {
            log.error(ExceptionCodeConstants.EMPTY_RESPONSE_FROM_UPI_PROFILE);
            throw exceptionBuilder.build(M2PRootExceptionCodeConstants.REMOTE_CALL_EMPTY_RESPONSE, new String[]{M2PRootExceptionMessageConstants.INTERNAL_ERROR_OCCURRED_MESSAGE}, Locale.US, null);
        }
        return response;
    }

    private HttpHeaders setHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
