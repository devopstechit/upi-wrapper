package com.upi.wrapper.contants.common;

public class ExceptionCodeConstants {

    public static final String EMPTY_RESPONSE_FROM_UPI_PROFILE = "empty.response.from.upi.profile";


    private ExceptionCodeConstants() {
    }
}
