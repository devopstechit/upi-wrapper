package com.upi.wrapper.controller.transaction;

import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.transaction.CheckBalanceReqDto;
import com.upi.common.dto.request.transaction.CheckTransactionReqDto;
import com.upi.common.dto.request.transaction.ReqPayDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.transaction.CheckBalanceRespDto;
import com.upi.common.dto.response.transaction.CheckTxnStatusRespDto;
import com.upi.common.dto.response.transaction.ReqPayRespDto;
import com.upi.common.enums.ApiResponseStatus;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.service.impl.TransactionServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@Slf4j
@RestController
@RequestMapping(value = "upi/v1/wrapper/transaction")
public class TransactionController {
    @Autowired
    ApiExceptionBuilder exceptionBuilder;

    @Autowired
    TransactionServiceImpl transactionService;

    @PostMapping(value = "checkTxnReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<AbstractResponse>> checkTxnReq(@Valid @RequestBody CheckTransactionReqDto request, Errors errors) {
        try {
            log.info("Request received for checkTxnReq: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.checkTxnReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkTxnReq: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<AbstractResponse>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "checkTxnRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<CheckTxnStatusRespDto>> checkTxnRes(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for checkTxnRes: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.checkTxnRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkTxnRes: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<CheckTxnStatusRespDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "reqPayReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<AbstractResponse>> reqPayReq(@Valid @RequestBody ReqPayDto request, Errors errors) {
        try {
            log.info("Request received for reqPayReq: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.reqPayReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in reqPayReq: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<AbstractResponse>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "reqPayRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<ReqPayRespDto>> reqPayRes(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for reqPayRes: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.reqPayRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in reqPayRes: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<ReqPayRespDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkBalanceReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<AbstractResponse>> checkBalanceReq(@Valid @RequestBody CheckBalanceReqDto request, Errors errors) {
        try {
            log.info("Request received for checkBalanceReq: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.checkBalanceReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkBalanceReq: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<AbstractResponse>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkBalanceRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<CheckBalanceRespDto>> checkBalanceRes(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for checkBalanceRes: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(transactionService.checkBalanceRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkBalanceRes: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<CheckBalanceRespDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
