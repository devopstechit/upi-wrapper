package com.upi.wrapper.controller.generic;

import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.profile.ListKeysDto;
import com.upi.common.dto.response.AbstractResponse;
import com.upi.common.dto.response.bank.AccPvdRespDto;
import com.upi.common.dto.response.bank.BankDto;
import com.upi.common.dto.response.util.CheckSimBindResponseDto;
import com.upi.common.dto.response.util.SimBindReqResponseDto;
import com.upi.common.enums.ApiResponseStatus;
import com.upi.common.enums.BooleanType;
import com.upi.common.schema.RespListKeys;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.service.impl.BankServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "upi/v1/wrapper/util/")
public class UtilityController {

    @Autowired
    ApiExceptionBuilder exceptionBuilder;

    @Autowired
    BankServiceImpl bankService;

    @PostMapping(value = "listBanks", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<List<BankDto>>> listBanks(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            UpiResponse<List<BankDto>> resp = bankService.listBanks(request);
            return new ResponseEntity<>(resp, HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<List<BankDto>>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "listAccPvd", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<List<AccPvdRespDto>>> listAccountProviders(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.listAccountProviders(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<List<AccPvdRespDto>>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkDevice", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<BooleanType>> checkDevice(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for check device: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.checkDevice(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in check device: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<BooleanType>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "listKeysReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<AbstractResponse>> listKeysReq(@Valid @RequestBody ListKeysDto request, Errors errors) {
        try {
            log.info("Request received for list keys request: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.listKeysReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in list keys request: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<AbstractResponse>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "listKeysRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<RespListKeys.KeyList>> listKeysRes(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for list keys response: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.listKeysRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in list keys response: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<RespListKeys.KeyList>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkSimBindStatus", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<CheckSimBindResponseDto>> checkSimBindStatus(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for check sim bind status: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.checkSimBindStatus(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in check sim bind status: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<CheckSimBindResponseDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "simBindReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<SimBindReqResponseDto>> simBindReq(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for sim bind request: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(bankService.simBindReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in sim bind request: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<SimBindReqResponseDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
