package com.upi.wrapper.controller.profile;

import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.upi.common.dto.request.AbstractRequestDto;
import com.upi.common.dto.request.account.AccountFetchReqDto;
import com.upi.common.dto.request.npci.AccountDto;
import com.upi.common.dto.request.npci.AccountReqDto;
import com.upi.common.dto.request.npci.AddressDto;
import com.upi.common.dto.request.profile.BindSimDto;
import com.upi.common.dto.request.profile.ProfileDto;
import com.upi.common.dto.request.vpa.VpaReqDto;
import com.upi.common.dto.response.npci.AccountFetchResDto;
import com.upi.common.enums.ActionType;
import com.upi.common.enums.ApiResponseStatus;
import com.upi.common.enums.BooleanType;
import com.upi.common.schema.RespTypeValAddr;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.service.impl.ProfileServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "upi/v1/wrapper/profile/")
public class ProfileController {

    @Autowired
    ApiExceptionBuilder exceptionBuilder;

    @Autowired
    ProfileServiceImpl profileService;

    @PostMapping(value = "listAccounts", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<List<AccountDto>>> fetchAccounts(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for fetch accounts: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.listAccounts(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<List<AccountDto>>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "createVpa", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> addNewVpa(@Valid @RequestBody VpaReqDto request, Errors errors) {
        try {
            log.info("Request received to create upi: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.addNewVpa(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "vpa/{action}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> vpaAction(@Valid @RequestBody VpaReqDto request, Errors errors, @PathVariable ActionType action) {
        try {
            log.info("Request received to create upi: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.vpaAction(request, action.getValue()), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "account/{action}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> accountAction(@Valid @RequestBody AccountReqDto request, Errors errors, @PathVariable ActionType action) {
        try {
            log.info("Request received to create upi: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.accountAction(request, action.getValue()), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkUpiPort", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> checkUpiPort(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received check upi port: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.checkUpiPort(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "createUpiPort", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> createUpiPort(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received to create upi port: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.createUpiPort(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "linkAccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> linkAccount(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.linkAccount(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "accountFetchReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> accountFetchReq(@Valid @RequestBody AccountFetchReqDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.accountFetchReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "accountFetchRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<AccountFetchResDto>> accountFetchRes(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.accountFetchRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<AccountFetchResDto>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "getAccountDetails", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> getAccountDetails(@Valid @RequestBody AbstractRequestDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.getAccountDetails(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "addAccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> addAccount(@Valid @RequestBody AccountReqDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.addAccount(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "setPrimaryAccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> setPrimaryAccount(@Valid @RequestBody AccountReqDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.setPrimaryAccount(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "deleteAccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> deleteAccount(@Valid @RequestBody AccountReqDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.deleteAccount(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "setAsPrimaryVpa", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> setPrimaryUpiId(@Valid @RequestBody VpaReqDto request, Errors errors) {
        try {
            log.info("Request received for fetch banks: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.setPrimaryUpiId(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in fetch banks: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "validateAddReq", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> validateAddReq(@Valid @RequestBody AddressDto request, Errors errors) {
        try {
            log.info("Request received for validateAddReq: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.validateAddReq(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in validateAddReq: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "validateAddRes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<RespTypeValAddr>> validateAddRes(@Valid @RequestBody ProfileDto request, Errors errors) {
        try {
            log.info("Request received for validateAddRes: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.validateAddRes(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in validateAddRes: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<RespTypeValAddr>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "checkIfVpaAvailable", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> checkIfVpaAvailable(@Valid @RequestBody VpaReqDto request, Errors errors) {
        try {
            log.debug("Request received for checkIfVpaAvailable: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.checkIfVpaAvailable(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkIfVpaAvailable: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "bindSim", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<BooleanType>> bindSim(@Valid @RequestBody BindSimDto request, Errors errors) {
        try {
            log.debug("Request received for bindSim: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(profileService.bindSim(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in bindSim: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<BooleanType>builder().status(ApiResponseStatus.FAILURE).exception(e).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
