package com.upi.wrapper;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@OpenAPIDefinition(info = @Info(title = "UPI-Wrapper", version = "1.0.0", description = "Project to manage api's related to UPI Services"))
public class UpiWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpiWrapperApplication.class, args);
    }

}
